import Vue from 'vue'
import Vuex from 'vuex'
import productSearch from './modules/productSearch';
import shoppingCart from "./modules/shoppingCart";
import auth from "./modules/auth";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    backend: 'https://naijamart-laravel.herokuapp.com/api',
    awsUrl: 'https://naijamart.s3.amazonaws.com/'
  },
  getters: {
    backend: state => {
      return state.backend;
    },
    awsUrl: state => {
      return state.awsUrl;
    }
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    productSearch,
    shoppingCart,
    auth
  }
})
