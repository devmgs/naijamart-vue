const state = {
    auth_user_role: localStorage.getItem('auth_user_role') || null,
    access_token: localStorage.getItem('access_token') || null,
};

const getters = {
    access_token(state) {
        return state.access_token;
    },
    authUser(state) {
        return {
            role: state.auth_user_role
        }
    },
    authCheck(state) {
        return state.access_token !== null;
    }
};

const mutations = {
    setAccessToken(state, token) {
        state.access_token = token;
        localStorage.setItem('access_token', token);
    },
    setAuthUser(state, user) {
        state.auth_user_role = user.role;
        localStorage.setItem('auth_user_role', user.role);
    },
    destroyTokenAndAuthUser(state) {
        state.access_token = null;
        state.auth_user_role = null;
        localStorage.removeItem('access_token');
        localStorage.removeItem('auth_user_role');
    },
};

const actions = {
    retrieveToken(context, payload) {
        return new Promise((resolve, reject) => {
            axios.post('/api/login', payload).then(response => {
                context.commit('setAccessToken', response.data.access_token);
                resolve(response);
            }).catch(error => {
                reject(error);
            })
        })
    },
    getAuthUser(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.access_token;
        return new Promise((resolve, reject) => {
            axios.get('/api/user').then(userApiResponse => {
                let user = userApiResponse.data;
                context.commit('setAuthUser', user);
                resolve(userApiResponse);
            });
        });
    },
    logout(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.access_token;
        return new Promise((resolve, reject) => {
            axios.post('/api/logout').then(response => {
                context.commit('destroyTokenAndAuthUser');
                resolve(response);
            })
        })
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
