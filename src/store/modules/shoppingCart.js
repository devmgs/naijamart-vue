const state = {
    products: [],
};

const getters = {
    productCount: state => {
        return state.products.length;
    },
    products: state => {
        return state.products;
    }
};

const mutations = {
    addToCart(state, product) {
        state.products.push(product);
    },
    removeFromCart(state, productIndex) {
        state.products.splice(productIndex, 1);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations
};

