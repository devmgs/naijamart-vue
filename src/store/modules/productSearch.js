const algoliasearch = require('algoliasearch');
const client = algoliasearch('UFM95IRJI9', '540b72b07212cd0eff9b53231133ed19');
const index = client.initIndex('products');
const state = {
    searchResults: [],
    stateID: null,
    searchHits: [],
    query: null,
};
const getters = {
    searchResults: state => {
        return state.searchResults;
    },
    stateID: state => {
        return state.stateID
    },
    searchHits: state => {
        return state.searchHits
    },
    query: state => {
        return state.query
    }
};
const mutations = {
    updateSearchQuery(state, query) {
        state.query = query;
    },
    updateStateID(state, selectedStateID) {
        state.stateID = selectedStateID;
    },
    setReturnedHits(state, returnedHits) {
        state.searchHits = returnedHits;
    },
    clearHits(state) {
        state.searchHits = [];
    }
};
const actions = {
    searchProducts(context) {
        index.search(context.getters.query, {
            filters: '(stateIDs=' + context.getters.stateID + ')'
        }).then(response => {
            context.commit('setReturnedHits', response.hits);
            let suggestionBox = document.getElementById('hits-container');
            if(suggestionBox) {
                suggestionBox.style.display = "block";
            }
        })
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
};
