import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/product-catalog',
        name: 'Product Catalog',
        component: () => import(/* webpackChunkName: "product-catalog" */ '../views/ProductsCatalog')
    },
    {
      path: '/trader/:traderID/products',
      name: 'Trader Catalog',
        component: () => import(/* webpackChunkName: "trader-catalog" */ '../views/BrandCatalog')
    },
    {
      path: '/products/:productID',
        name: 'Product Details',
        component: () => import(/* webpackChunkName: "product-details" */ '../views/ProductDetails')
    },
    {
        path: '/checkout',
        name: 'Checkout',
        component: () => import(/* webpackChunkName: "checkout" */'../views/Checkout')
    },
    {
        path: '/about',
        name: 'About',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
