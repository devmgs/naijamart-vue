import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import '@mdi/font/css/materialdesignicons.css'

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
            light: {
                secondary: '#E9E3CF',
                darkColor: '#E7C16D',
                tertiary: '#141414',
                quaternary: '#F9F8EE',
                lightColor: '#FFFFFF',
                primary: '#000000'
            }
        }
    },
    icons: {
        iconfont: 'mdi', // default - only for display purposes
    },
});
